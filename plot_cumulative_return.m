ret = ret_Value_npeb_ar(:,2);
len = 150;

for iter = 1:len
    cumulative_ret(iter) = sum(ret(1:iter));
end

plot(exp(cumulative_ret));
