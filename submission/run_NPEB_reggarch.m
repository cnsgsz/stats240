warning off;
MAXFUNEVAL = 100;
winsize= 100;

lambdas=2.^( (-3):1:11 );
eta=1.0:0.5:10;
B = 40;
nStocks=6;
lb= ones(nStocks, 1)*(-0.05);		ub=ones(nStocks, 1);

file = load('FF6Portfolios.txt');
allmonth = file(:,1);

nPeriod=length(allmonth) - winsize;
sharpe_train=zeros(nPeriod, 1);
res_sharpe=zeros(nPeriod, length(lambdas));
res_rets=zeros(nPeriod, length(lambdas));
ret_Value_npeb_srgar = zeros(nPeriod, 3);

for j = 1 : nPeriod
    data = file(j : end, [2, 5, 8, 11, 14, 17]);
    Xtrain = data(1:winsize, :);
    Xtest = data(winsize + 1, :);
    
    for lam=1:length(lambdas)
        lambda = lambdas(lam);
        bi = randint(B, winsize-1, [1, winsize-1]);

    %%% Use Sto-Reg-GARCH model for each series
        [coeff, stdinno, sigmas, fitted, meanPred, ...
            secPred]=fitStoRegGARCH(Xtrain, winsize);   
        for b=1:B
            tmpinno = stdinno(bi(b,:), :);    
            bootsample = [Xtrain(1,:); fitted+(tmpinno.*sigmas)];
            [tmpcoeff, tmpinno, tmpsigmas, tmpfitted, tmpmeanPred, ...
                tmpsecPred] = fitStoRegGARCH(bootsample, winsize);
            for k=1:length(eta)
                tmpwt = getOptWt_Quadprog(tmpmeanPred',tmpsecPred,...
                    lambda/eta(k),lb,ub);
                C1(k,b) = tmpmeanPred'*tmpwt;
                C2(k,b) = tmpwt'*tmpsecPred*tmpwt;
            end
        end    
        indb = find( (~(mean(C1)>-1000) == 1) & (~(mean(C2)>-1000) == 1));
        if (length(indb)>0) 
            C1(:,indb)=0;   C2(:,indb)=0;
        end
        ind=find(mean(C1,1)<10^20 & mean(C2,1)<10^20);
        C1=C1(:,ind);   C2=C2(:,ind);
        Cfun=mean(C1,2)-lambda*mean(C2,2)+lambda*mean(C1,2).^2;
    
        opteta = eta(Cfun==max(Cfun));
        if (length(opteta)>1)  opteta=opteta(1);   end
        wts = getOptWt_Quadprog(meanPred,secPred,lambda/opteta, lb, ub);
        ret_train = Xtrain*wts;
        sharpe_train = mean(ret_train)/std(ret_train);
        rets =Xtest*wts;

        if lam==1
            maxsharpe=sharpe_train;  rets_maxsharpe=rets;
        end
        if maxsharpe<= sharpe_train
            maxsharpe = sharpe_train; rets_maxsharpe = rets;
        else
            break;
        end
        [j, lam, maxsharpe, rets_maxsharpe]
    end
    ret_Value_npeb_srgar(j,:) = [lam, maxsharpe, rets_maxsharpe];
end

