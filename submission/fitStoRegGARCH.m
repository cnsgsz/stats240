
function [coeff, stdinno, sigmas, fitted, meanPred, ...
    secPred] = fitStoRegGARCH(Xtrain, winsize)

nStocks = size(Xtrain, 2);
coeff = zeros(nStocks, 5);
inno = zeros( winsize-1, nStocks);
stdinno = zeros( winsize-1, nStocks);
sigmas = inno;      fitted = inno;
covPred = zeros(nStocks,nStocks);    

spec=garchset('R', 1, 'M', 0, 'P', 1, 'Q', 1, 'display', 'off');
for ind = 1:nStocks
    regMat = [Xtrain(1:(winsize-1), ind)']';
    regY = Xtrain(2:winsize, ind);
    [tmp,err,llf,inno(:,ind),sigmas(:,ind),...
            summary]=garchfit(spec,regY,regMat);
    coeff(ind,:)=[tmp.C,tmp.Regress,tmp.K,tmp.GARCH,tmp.ARCH];
    fitted(:,ind) = regMat*coeff(ind,2)'+coeff(ind,1);
    covPred(ind,ind) = tmp.K + tmp.GARCH*sigmas(end,ind)^2 ...
            + tmp.ARCH*inno(end,ind)^2;
end
stdinno = inno./sigmas;
meanPred = coeff(:,1) + sum(coeff(:,2).*[Xtrain(winsize,:)'],2);

for ind1 = 2:nStocks
    for ind2 = 1:(ind1-1)
        covPred(ind1,ind2)=sqrt(covPred(ind1,ind1)*covPred(ind2,ind2))...
            *corr(stdinno(:,ind1),stdinno(:,ind2));
        covPred(ind2,ind1)=covPred(ind1, ind2);
    end
end
secPred = covPred + meanPred'*meanPred;


    
